package ph.otomatik.openweather.utils;

import ph.otomatik.openweather.pojo.Climate;
import ph.otomatik.openweather.pojo.ClimateList;
import retrofit2.Call;
import retrofit2.http.GET;

import retrofit2.http.Query;

public interface Api {

    @GET("group")
    Call<ClimateList> getWeatherByGroup(@Query("id") String place, @Query("APPID") String key);

    @GET("weather")
    Call<Climate> getWeatherById(@Query("id") String place, @Query("APPID") String key);


}
