package ph.otomatik.openweather.listener;

import android.view.View;

public interface RecyclerClickListener {
    void onItemClick(View v, int position);
}
