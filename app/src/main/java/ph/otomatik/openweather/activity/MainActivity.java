package ph.otomatik.openweather.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.otomatik.openweather.R;
import ph.otomatik.openweather.adapter.WeatherAdapter;
import ph.otomatik.openweather.listener.RecyclerClickListener;
import ph.otomatik.openweather.pojo.Climate;
import ph.otomatik.openweather.pojo.ClimateList;
import ph.otomatik.openweather.utils.Api;
import ph.otomatik.openweather.utils.ApiSync;
import ph.otomatik.openweather.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private final String TAG = getClass().getSimpleName();
    @BindView(R.id.rcy_weather)
    RecyclerView recyclerWeather;
    @BindView(R.id.lblNetwork)
    TextView lblNetwork;
    private Api api;
    private List<Climate> climates;
    private WeatherAdapter weatherAdapter;
    private boolean isFailedOnload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        api = ApiSync.getClient().create(Api.class);

        Call<ClimateList> call = api.getWeatherByGroup(Constants.DEFAULT_CITIES, Constants.KEY);
        call.enqueue(new Callback<ClimateList>() {
            @Override
            public void onResponse(Call<ClimateList> call, Response<ClimateList> response) {
                ClimateList climateList = response.body();
                climates = climateList.getClimateList();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lblNetwork.setVisibility(View.INVISIBLE);
                        weatherAdapter = new WeatherAdapter(climates, new RecyclerClickListener() {
                            @Override
                            public void onItemClick(View v, int position) {
                                Climate climate = climates.get(position);
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(Constants.WEATHER_KEY, climate);
                                Intent intent = new Intent(MainActivity.this, WeatherDetailsActivity.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        });
                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerWeather.setLayoutManager(layoutManager);
                        recyclerWeather.setAdapter(weatherAdapter);
                    }
                });

            }

            @Override
            public void onFailure(Call<ClimateList> call, Throwable t) {
                Log.d(TAG, "ONFAILURE " + t.getLocalizedMessage());
                lblNetwork.setVisibility(View.VISIBLE);
                isFailedOnload = true;
            }
        });
    }

    @OnClick(R.id.btnListRefresh)
    public void refreshList() {
        Call<ClimateList> call = api.getWeatherByGroup(Constants.DEFAULT_CITIES_TWO, Constants.KEY);
        call.enqueue(new Callback<ClimateList>() {
            @Override
            public void onResponse(Call<ClimateList> call, Response<ClimateList> response) {
                ClimateList climateList = response.body();
                climates = climateList.getClimateList();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        lblNetwork.setVisibility(View.INVISIBLE);
                        weatherAdapter = new WeatherAdapter(climates, new RecyclerClickListener() {
                            @Override
                            public void onItemClick(View v, int position) {
                                Climate climate = climates.get(position);
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(Constants.WEATHER_KEY, climate);
                                Intent intent = new Intent(MainActivity.this, WeatherDetailsActivity.class);
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        });
                        if (isFailedOnload){
                            Log.d(TAG, "WTF");
                            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                            recyclerWeather.setLayoutManager(layoutManager);
                            recyclerWeather.setAdapter(weatherAdapter);
                        }else {
                            Log.d(TAG, "WTF2");
                            recyclerWeather.swapAdapter(weatherAdapter, false);
                        }
                    }
                });

            }

            @Override
            public void onFailure(Call<ClimateList> call, Throwable t) {
                Log.d(TAG, "ONFAILURE " + t.getLocalizedMessage());
                lblNetwork.setVisibility(View.VISIBLE);

            }
        });
    }

}
