package ph.otomatik.openweather.utils;

public interface Constants {
    String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    String ICON_URL = "http://openweathermap.org/img/w/";
    String KEY = "343480e020b17aaf350d75de8abdfe05";
    String WEATHER_KEY = "weather";
    int BASE_ZERO = 0;
    String DEFAULT_CITIES = "5391997,3067696,2643743,1701668";
    String DEFAULT_CITIES_TWO = "5391997,3067696,2643743";
}
