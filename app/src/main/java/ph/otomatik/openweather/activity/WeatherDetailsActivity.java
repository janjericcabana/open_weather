package ph.otomatik.openweather.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ph.otomatik.openweather.R;
import ph.otomatik.openweather.pojo.Climate;
import ph.otomatik.openweather.pojo.Weather;
import ph.otomatik.openweather.utils.Api;
import ph.otomatik.openweather.utils.ApiSync;
import ph.otomatik.openweather.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherDetailsActivity extends AppCompatActivity {

    private String TAG = getClass().getSimpleName();
    private Climate climate;
    private Api api;

    @BindView(R.id.lblWeatherCity)
    TextView lblCity;
    @BindView(R.id.lblWeatherDescription)
    TextView lblWeatherDescription;
    @BindView(R.id.imgWeatherIcon)
    ImageView imgWeatherIcon;
    @BindView(R.id.lblWeatherTag)
    TextView lblWeatherTag;
    @BindView(R.id.lblWeatherNetwork)
    TextView lblWeatherNetwork;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_details);
        ButterKnife.bind(this);
        api = ApiSync.getClient().create(Api.class);
        Bundle bundle = getIntent().getExtras();
        climate = (Climate) bundle.getSerializable(Constants.WEATHER_KEY);

        setWeatherDetails();

    }

    @OnClick(R.id.btnRefresh)
    public void refresh(){
        Call<Climate> call = api.getWeatherById(climate.getCityId(), Constants.KEY);
        call.enqueue(new Callback<Climate>() {
            @Override
            public void onResponse(Call<Climate> call, Response<Climate> response) {
                lblWeatherNetwork.setVisibility(View.INVISIBLE);
                climate = response.body();
                setWeatherDetails();
            }

            @Override
            public void onFailure(Call<Climate> call, Throwable t) {
                lblWeatherNetwork.setVisibility(View.VISIBLE);
                climate = null;
                setWeatherDetails();
            }
        });
    }

    public void setWeatherDetails(){
        Weather weather = null;
        if (climate != null) {
             weather = climate.getWeather().get(Constants.BASE_ZERO);
            Glide.with(this)
                    .load(Constants.ICON_URL + weather.getIcon() + ".png")
                    .apply(new RequestOptions().override(200))
                    .into(imgWeatherIcon);

        }
            lblCity.setText(climate != null ? climate.getCity(): "City");
            lblWeatherDescription.setText(climate != null ? weather.getDescription() : "Description");
            lblWeatherTag.setText(climate != null ? weather.getMain() : "Tag" );

    }
}
