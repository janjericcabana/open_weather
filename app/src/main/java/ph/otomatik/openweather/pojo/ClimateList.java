package ph.otomatik.openweather.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ClimateList {
    @SerializedName("list")
    private List<Climate> climateList;

    public List<Climate> getClimateList() {
        return climateList;
    }

    public void setClimateList(List<Climate> climateList) {
        this.climateList = climateList;
    }
}
