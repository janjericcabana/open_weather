package ph.otomatik.openweather.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.otomatik.openweather.R;
import ph.otomatik.openweather.listener.RecyclerClickListener;
import ph.otomatik.openweather.pojo.Climate;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.Holder>{

    private List<Climate> climateList;
    private RecyclerClickListener clickListener;

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.lblCity)
        TextView lblCity;
        @BindView(R.id.lblWeather)
        TextView lblWeather;
        @BindView(R.id.lblTemperature)
        TextView lblTemperature;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onItemClick(view, getAdapterPosition());
                }
            });
        }
    }

    public WeatherAdapter(List<Climate> climateList, RecyclerClickListener clickListener) {
        this.climateList = climateList;
        this.clickListener = clickListener;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_weather, viewGroup, false);
        return new Holder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Climate climate = climateList.get(position);
        holder.lblCity.setText(climate.getCity());
        holder.lblWeather.setText(climate.getWeather().get(0).getDescription());
        holder.lblTemperature.setText(String.valueOf(climate.getMain().getTemperature()));
    }

    @Override
    public int getItemCount() {
        return climateList.size();
    }


}
